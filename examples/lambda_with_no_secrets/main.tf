# data "terraform_remote_state" "infra" {
#   backend = "remote"

#   config = {
#     organization = "MobileHub"
#     workspaces = {
#       name = "mobilehub-infra-${terraform.workspace}"
#     }
#   }
# }

locals {
  # Update expressions correctly later!
  cronjobs = [
    {
      cronjob_name        = "stock_inventory",
      cronjob_description = "",
      cronjob_expression  = "cron(0 20 * * ? *)",
      cronjob_handler     = "",
      cronjob_timeout     = "600",
      cronjob_command     = "/var/www/bin/console app:tracking:update"
    },
    {
      cronjob_name        = "tracking_update",
      cronjob_description = "",
      cronjob_expression  = "cron(0 20 * * ? *)",
      cronjob_handler     = "",
      cronjob_timeout     = "600",
      cronjob_command     = "/var/www/bin/console app:tracking:update"
    },
    {
      cronjob_name        = "scraping_gsm",
      cronjob_description = "",
      cronjob_expression  = "cron(0 20 * * ? *)",
      cronjob_handler     = "",
      cronjob_timeout     = "600",
      cronjob_command     = "/var/www/bin/console app:scraping-gsm"
    },
    {
      cronjob_name        = "azure_sync_cron",
      cronjob_description = "",
      cronjob_expression  = "cron(0 20 * * ? *)",
      cronjob_handler     = "",
      cronjob_timeout     = "600",
      cronjob_command     = "/var/www/bin/console app:azure-sync-cron"
    },
    {
      cronjob_name        = "return_reminder",
      cronjob_description = "",
      cronjob_expression  = "cron(0 20 * * ? *)",
      cronjob_handler     = "",
      cronjob_timeout     = "600",
      cronjob_command     = "/var/www/bin/console app:return:reminder"
    },
  ]

  cronjob_allowed_secrets_arn = []

  image_uri = "" # Updated your Docker Image URI here!
}

provider "aws" {
  region = "us-east-1"

  # Make it faster by skipping something
  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
  skip_requesting_account_id  = true

  profile = "default"
}

/* ------------------------------------------------------------------------------------------------------------ */
/* 2. Create IAM Role + Role Policy for IAM Role                                                                */
/* ------------------------------------------------------------------------------------------------------------ */

resource "aws_iam_role" "iam_for_lambda" {
  name = "cronjob"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

module "lambda_docker_cronjob_stock_inventory" {
  source = "../../"

  # count = length(local.cronjobs)
  for_each = toset(keys({ for i, r in local.cronjobs : i => r }))

  iam_role_name             = aws_iam_role.iam_for_lambda.arn
  role_permissions_boundary = null

  ecs_security_group_ids = []
  private_subnet_ids     = []

  aws_lambda_permission_statement_id = "Lambda_Cronjob_Statement"

  cronjob_name        = local.cronjobs[each.value]["cronjob_name"]
  cronjob_description = local.cronjobs[each.value]["cronjob_description"]
  cronjob_expression  = local.cronjobs[each.value]["cronjob_expression"]

  cronjob_handler = local.cronjobs[each.value]["cronjob_handler"]
  cronjob_timeout = local.cronjobs[each.value]["cronjob_timeout"]

  image_config_entry_point       = ["sh", "entrypoint.sh"] # Entrypoint override
  image_config_command           = ["php", local.cronjobs[each.value]["cronjob_command"]]
  image_config_working_directory = "/"

  image_uri = local.image_uri

  lambda_function_name = "${local.cronjobs[each.value]["cronjob_name"]}-cronjob"

  lambda_env_variables = {
    EMAILS_ALERT      = "",
    MARIADB_USER      = "",
    MARIADB_PASSWORD  = "",
    MARIADB_HOST      = "",
    MARIADB_PORT      = "",
    MARIADB_DB        = "",
    APP_SECRET        = "",
    CHR_ACCOUNT       = "",
    CHR_PWD           = "",
    AFTERSHIP_KEY     = "",
    RABBITMQ_USER     = "",
    RABBITMQ_PASSWORD = "",
    RABBITMQ_ENDPOINT = "",
    SECRET_KEY        = "",
    MAILER_DSN        = ""

  }
  lambda_description = "Execute cronjob for ${local.cronjobs[each.value]["cronjob_name"]}"

  tags = {
    "namespace" = "dev"
  }

  target_id = "Cronjob_event_target_${local.cronjobs[each.value]["cronjob_name"]}"

}
